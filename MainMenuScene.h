//
//  MainMenuScene.h
//  AllMine
//
//  Created by Benjamin Jordan on 7/14/12.
//  Copyright 2012 TheGoldenMule. All rights reserved.
//

#import <Foundation/Foundation.h>
#import "cocos2d.h"

@interface MainMenuScene : CCScene {
    
}

+(MainMenuScene *)scene;

@end