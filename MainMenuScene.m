//
//  MainMenuScene.m
//  AllMine
//
//  Created by Benjamin Jordan on 7/14/12.
//  Copyright 2012 TheGoldenMule. All rights reserved.
//

#import "MainMenuScene.h"
#import "GameScene.h"
#import "ProgressScene.h"

@interface MainMenuScene (Private)

-(void)createMenu;

-(void)quickPlay;
-(void)singlePlayer;
-(void)multiPlayer;

@end


@implementation MainMenuScene

+(MainMenuScene *)scene {
    return [[[MainMenuScene alloc] init] autorelease];
}

-(id) init {
    if ((self = [super init])) {
        [self createMenu];
    }
    
    return self;
}

-(void) dealloc {
    CCLOG(@"Deallocating %@", self);
    
    [super dealloc];
}

#pragma mark Private

-(void)createMenu {
    CCMenuItemLabel *quickPlay = [CCMenuItemLabel itemWithLabel:[CCLabelTTF labelWithString:@"Quick-Play" fontName:@"Helvetica" fontSize:20]  target:self selector:@selector(quickPlay)];
    quickPlay.position = ccp(quickPlay.position.x, quickPlay.position.y + 30);
    
    CCMenuItemLabel *singlePlayer = [CCMenuItemLabel itemWithLabel:[CCLabelTTF labelWithString:@"Single Player" fontName:@"Helvetica" fontSize:20]  target:self selector:@selector(singlePlayer)];
    
    CCMenuItemLabel *multiPlayer = [CCMenuItemLabel itemWithLabel:[CCLabelTTF labelWithString:@"Multi Player" fontName:@"Helvetica" fontSize:20]  target:self selector:@selector(multiPlayer)];
    multiPlayer.position = ccp(multiPlayer.position.x, multiPlayer.position.y - 30);
    
    [self addChild:[CCMenu menuWithItems:quickPlay, singlePlayer, multiPlayer, nil]];
}

-(void)quickPlay {
    [[CCDirector sharedDirector] replaceScene:[GameScene sceneWithRandomLevel]];
}

-(void)singlePlayer {
    [[CCDirector sharedDirector] replaceScene:[ProgressScene scene]];
}

-(void)multiPlayer {
    [[CCDirector sharedDirector] replaceScene:[GameScene sceneWithRandomLevel]];
}

@end