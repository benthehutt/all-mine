//
//  GridLayer.h
//  AllMine
//
//  Created by Benjamin Jordan on 7/7/12.
//  Copyright (c) 2012 TheGoldenMule. All rights reserved.
//

#import "cocos2d.h"
#import "CCLayer.h"
#import "GridModel.h"

@interface GridLayer : CCLayer <CCTargetedTouchDelegate> {
    GridModel *_grid;
    Array2 *_sprites;
    CCSpriteBatchNode *_batchNode;
    
    int _depth;
    NodeModel *_searchModel;
    
    UITouch *_startDragTouch;
    CGPoint _startDragLocation;
    BOOL _moved;
    
    CGPoint _lastLocation;
    CGPoint _velocity;
}

@property (readonly) GridModel *grid;

-(void)build:(NSDictionary *)properties;
-(void)activateNodesAbout:(NodeModel *)model searchDepth:(int)depth;
-(BOOL)isClearOfMines;

-(BOOL)ccTouchBegan:(UITouch *)touch withEvent:(UIEvent *)event;
-(void)ccTouchMoved:(UITouch *)touch withEvent:(UIEvent *)event;
-(void)ccTouchEnded:(UITouch *)touch withEvent:(UIEvent *)event;
-(void)ccTouchCancelled:(UITouch *)touch withEvent:(UIEvent *)event;

@end
