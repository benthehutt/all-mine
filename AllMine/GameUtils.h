//
//  GameUtils.h
//  AllMine
//
//  Created by Benjamin Jordan on 7/9/12.
//  Copyright (c) 2012 TheGoldenMule. All rights reserved.
//

#import <UIKit/UIKit.h>
#import "cocos2d.h"

@interface GameUtils : NSObject

+(BOOL)nodeContainsTouchLocation:(UITouch *)touch target:(CCNode *)target;
+(BOOL)containsTouchLocation:(UITouch *)touch target:(CCNode *)target;
+(CGPoint)locationInNodeSpace:(UITouch *)touch target:(CCNode *)target;

@end
