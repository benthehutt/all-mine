//
//  GridLayer.m
//  AllMine
//
//  Created by Benjamin Jordan on 7/7/12.
//  Copyright (c) 2012 TheGoldenMule. All rights reserved.
//

#import "GridLayer.h"
#import "GridModel.h"
#import "NodeSprite.h"
#import "Array2.h"
#import "GameUtils.h"

@interface GridLayer (Private)
-(void)createNodeSprites;
-(void)activate:(NodeModel *)model;
-(void)moveMe:(float)x y:(float)y;
@end

@implementation GridLayer

@synthesize grid = _grid;

-(id) init {
    if ((self = [super init])) {
        // make sure this atlas is added to the cache
        [[CCSpriteFrameCache sharedSpriteFrameCache] addSpriteFramesWithFile:@"NodeAtlas.plist"];
        
        // create a batch node
        _batchNode = [[CCSpriteBatchNode batchNodeWithFile:@"NodeAtlas.png"] retain];
        [self addChild:_batchNode];
    }
    
    return self;
}

-(oneway void)dealloc {
    CCLOG(@"Deallocating %@", self);
    
    [_grid release];
    [_batchNode release];
    [_sprites release];
    
    [super dealloc];
}

-(void)build:(NSDictionary *)properties {
    CCLOG(@"Building GridLayer.");
    
    // create a new grid model
    [_grid release];
    _grid = [[GridModel alloc] initWithProperties:properties];
    
    // create node sprites
    [self createNodeSprites];
}

-(void)activateNodesAbout:(NodeModel *)model searchDepth:(int)depth {
    _searchModel = model;
    _depth = depth;
    
    NSMutableArray *queue = [NSMutableArray array];
    [self activate:model queue:queue];
    
    NSMutableArray *actions = [NSMutableArray array];
    NSEnumerator *enumerator = [queue objectEnumerator];
    NodeModel *pModel;
    while ((pModel = [enumerator nextObject])) {
        NodeSprite * sprite = [_sprites getAtX:pModel.x y:pModel.y];
        
        [actions addObject:[CCDelayTime actionWithDuration:0.03]];
        [actions addObject:[CCCallFunc actionWithTarget:sprite selector:@selector(invalidate)]];
    }
    [self runAction:[CCSequence actionsWithArray:actions]];
}

-(void)activate:(NodeModel *)model queue:(NSMutableArray *)queue{
    if (gkTypeMine == model.type) {
        return;
    }
    
    // return if we're over the distance
    float distanceSquared = (model.x - _searchModel.x) * (model.x - _searchModel.x)
        + (model.y - _searchModel.y) * (model.y - _searchModel.y);
    if (distanceSquared > _depth * _depth) {
        return;
    }
    
    // add me to the queue
    [queue addObject:model];
    
    // new state + active
    model.state = gkStateNeutral;
    model.active = true;
    
    // return if this is a non-zero valued node
    if (model.value != 0) return;
    
    NodeModel *it;
    NSArray *nodes = [_grid getPerimeterNodes:model radius:1];
    for (int i = 0, len = [nodes count]; i < len; i++) {
        it = ((NodeModel *)[nodes objectAtIndex:i]);
        if (gkStateUnknown == it.state
            && !it.active) {
            [self activate:it queue:queue];
        }
    }
}


-(BOOL)isClearOfMines {
    NodeModel *model;
    for (int x = 0, y = 0, width = _grid.width, height = _grid.height; x < width; x++) {
        for (y = 0; y < height; y++) {
            model = [_grid getModelAtX:x y:y];
            if (gkTypeMine != model.type && gkStateUnknown == model.state) {
                return NO;
            }
        }
    }
    
    return YES;
}

-(void)onEnter {
    [[CCTouchDispatcher sharedDispatcher] addTargetedDelegate:self priority:0 swallowsTouches:YES];
    [super onEnter];
}

-(void) onExit {
    [[CCTouchDispatcher sharedDispatcher] removeDelegate:self];
    [super onExit];
}

static const float drag = 0.05f;

-(void) update:(ccTime)deltaTime {
    // move by (velocity - drag * velocity)
    float velx = _velocity.x - drag * _velocity.x;
    float vely = _velocity.y - drag * _velocity.y;
    
    // clamp the values correctly (velocity could be > or < 0)
    if (_velocity.x < 0) {
        velx = clampf(velx, _velocity.x, 0);
    } else {
        velx = clampf(velx, 0, _velocity.x);
    }
    
    if (_velocity.y < 0) {
        vely = clampf(vely, _velocity.y, 0);
    } else {
        vely = clampf(vely, 0, _velocity.y);
    }
    
    // finally, move!
    _velocity = ccp(velx, vely);
    if (_velocity.x != 0 || _velocity.y != 0) {
        [self moveMe:self.position.x - _velocity.x y:self.position.y - _velocity.y];
    } else {
        // kill update
        [self unscheduleUpdate];
    }
}

#pragma mark CCTargetedTouchDelegate

-(BOOL)ccTouchBegan:(UITouch *)touch withEvent:(UIEvent *)event {
    // save start drag touch
    _startDragTouch = touch;
    _startDragLocation = [self convertTouchToNodeSpace:touch];
    _moved = NO;
    
    // kill updates
    [self unscheduleUpdate];
    
    return YES;
}

-(void)ccTouchMoved:(UITouch *)touch withEvent:(UIEvent *)event {
    if (_startDragTouch) {
        _lastLocation = [self convertTouchToNodeSpace:_startDragTouch];
        
        float diffx = _lastLocation.x - _startDragLocation.x;
        float diffy = _lastLocation.y - _startDragLocation.y;
        
        _velocity = ccp(-diffx, -diffy);
        
        if (_moved || fabsf(diffx + diffy) > 1.0f) {
            [self moveMe:self.position.x + diffx y:self.position.y + diffy];
            _moved = YES;
        }
    }
}

-(void)ccTouchEnded:(UITouch *)touch withEvent:(UIEvent *)event {
    BOOL canMove = nil != _startDragTouch;
    
    _startDragTouch = nil;
    
    if (canMove && !_moved) {
        // see if a nodesprite was clicked on
        NodeSprite *sprite;
        for (int x = 0, y = 0, width = _sprites.width, height = _sprites.height; x < width; x++) {
            for (y = 0; y < height; y++) {
                sprite = [_sprites getAtX:x y:y];
                if ([GameUtils nodeContainsTouchLocation:touch target:sprite]) {
                    // touched!
                    CCLOG(@"Touched Sprite.");
                    [[NSNotificationCenter defaultCenter] postNotification:[NSNotification notificationWithName:EVENT_NODE_SELECTED object:sprite]];
                    
                    // add particle emitter
                    CCParticleSystem *emitter = [ARCH_OPTIMAL_PARTICLE_SYSTEM particleWithFile:@"mine.plist"];
                    emitter.position = sprite.position;
                    [self addChild:emitter];
                    
                    return;
                }
            }
        }
    } else if (_moved) {
        // schedule update for
        [self scheduleUpdate];
    }
}

-(void)ccTouchCancelled:(UITouch *)touch withEvent:(UIEvent *)event {
    [self ccTouchEnded:touch withEvent:event];
}

#pragma mark Private

-(void)createNodeSprites {
    // create sprite model
    [_sprites release];
    _sprites = [[Array2 alloc] initWithWidth:_grid.width height:_grid.height];
    
    NodeSprite *sprite;
    NodeModel *model;
    for (int x = 0, width = _grid.width, height = _grid.height; x < width; x++) {
        for (int y = 0; y < height; y++) {
            model = [_grid getModelAtX:x y:y];
            sprite = [[[NodeSprite alloc]
                      initWithModel:model] autorelease];
            
            // add to sprites array2
            [_sprites setAtX:x y:y object:sprite];
            
            [self addChild:sprite];
        }
    }
}

-(void)moveMe:(float)x y:(float)y {
    CGSize size = [[CCDirector sharedDirector] winSize];
    self.position = ccp(
                        clampf(x, -CELL_DIM * _grid.width + size.width + 100, 0),
                        clampf(y, -CELL_DIM * _grid.height + size.height, 0));
}

@end