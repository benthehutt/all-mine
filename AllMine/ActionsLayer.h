//
//  ActionsLayer.h
//  AllMine
//
//  Created by Benjamin Jordan on 7/7/12.
//  Copyright 2012 TheGoldenMule. All rights reserved.
//

#import <Foundation/Foundation.h>
#import "cocos2d.h"
#import "RectangleLayer.h"

@interface ActionsLayer : CCLayer <CCTargetedTouchDelegate> {
    CCLabelTTF *_energy;
    CCLabelTTF *_biodiesel;
    CCLabelTTF *_levelName;
    CCSpriteBatchNode *_batchNode;
    NSArray *_nodeActions;
    NSArray *_actionInfoIcons;
    NSArray *_buttons;
    RectangleLayer *_bg;
    
    BOOL _isOpen;
    BOOL _isOpening;
    BOOL _isClosing;
}

-(void)build:(NSDictionary *)properties;
-(void)displayButtons:(NSArray *)actionButtons;

-(BOOL)ccTouchBegan:(UITouch *)touch withEvent:(UIEvent *)event;
-(void)ccTouchMoved:(UITouch *)touch withEvent:(UIEvent *)event;
-(void)ccTouchEnded:(UITouch *)touch withEvent:(UIEvent *)event;
-(void)ccTouchCancelled:(UITouch *)touch withEvent:(UIEvent *)event;

@end