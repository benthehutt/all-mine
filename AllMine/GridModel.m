//
//  GridModel.m
//  AllMine
//
//  Created by Benjamin Jordan on 7/7/12.
//  Copyright (c) 2012 TheGoldenMule. All rights reserved.
//

#import "GridModel.h"
#import "cocos2d.h"
#import "NodeModel.h"
#import "GlobalState.h"

@interface GridModel (Private)

-(void) generateRandomMines:(int)numberOfMines;
-(void) placeSpecificMines:(NSDictionary *)properties;
-(int) determineValue:(NodeModel *)model;

@end

@implementation GridModel

-(id) initWithProperties:(NSDictionary *)properties {
    if ((self = [super init])) {
        CCLOG(@"New %@", self);
        
        int width = [[properties valueForKey:@"grid_width"] intValue];
        int height = [[properties valueForKey:@"grid_height"] intValue];
        
        CCLOG(@"Gridsize: %i x %i", width, height);
        
        // create grid
        _grid = [[Array2 alloc] initWithWidth:width height:height];
        
        // populate grid with normal nodes
        for (int x = 0, lenx = _grid.width; x < lenx; x++) {
            for (int y = 0, leny = _grid.height; y < leny; y++) {
                [_grid setAtX:x y:y object:[[[NodeModel alloc] initAtX:x y:y] autorelease]];
            }
        }
        
        // mine generation
        if (nil != [properties valueForKey:@"numMines"]) {
            [self generateRandomMines:[[properties valueForKey:@"numMines"] intValue] * [GlobalState defaultInstance].difficulty];
        } else {
            [self placeSpecificMines:properties];
        }
        
        // now, generate values
        NodeModel *node;
        for (int x = 0, width = _grid.width, height = _grid.height; x < width; x++) {
            for (int y = 0; y < height; y++) {
                // get the node
                node = (NodeModel *)[_grid getAtX:x y:y];
                if (gkTypeMine != node.type) {
                    node.value = [self determineValue:node];
                }
            }
        }
    }
    
    return self;
}

-(void) dealloc {
    CCLOG(@"Deallocating %@", self);
    
    [_grid release];
    
    [super dealloc];
}

-(int)width {
    return _grid.width;
}

-(int)height {
    return _grid.height;
}

-(NodeModel *) getModelAtX:(int)x y:(int)y {
    return (NodeModel *)[_grid getAtX:x y:y];
}

-(NSArray *) getPerimeterNodes:(NodeModel *)model radius:(int)radius {
    return [_grid getPerimeterValues:model.x y:model.y radius:radius];
}

-(NSDictionary *)getDeployTotals {
    int biodiesel = 0;
    int energy = 0;
    NodeModel *model;
    for (int x = 0, y = 0, width = _grid.width, height = _grid.height; x < width; x++) {
        for (y = 0; y < height; y++) {
            model = [_grid getAtX:x y:y];
            
            if (nil == model.deployedAction) {
                continue;
            }
            
            if (model.deployCount > 0) {
                model.deployCount -= 1;
                
                biodiesel += model.value * [[model.deployedAction.properties objectForKey:@"fuelMultiplier"] intValue];
                energy += model.value * [[model.deployedAction.properties objectForKey:@"energyMultiplier"] intValue];
            } else {
                model.deployedAction = nil;
            }
        }
    }
    
    return [NSDictionary dictionaryWithObjectsAndKeys:
            [NSNumber numberWithInt:biodiesel], @"biodiesel",
            [NSNumber numberWithInt:energy], @"energy", nil];
}

-(NodeModel *)findSafePatch {
    NodeModel *model;
    for (int x = 0, y = 0, width = _grid.width, height = _grid.height; x < width; x++) {
        for (y = 0; y < height; y++) {
            model = [_grid getAtX:x y:y];
            if (gkTypeNormal == model.type
                && 0 == model.value) return model;
        }
    }
    
    return nil;
}

#pragma mark Private

-(void) generateRandomMines:(int)numberOfMines {
    CCLOG(@"Generating %i mines", numberOfMines);
    
    // randomly add some mines
    for (int i = 0; i < numberOfMines; i++) {
        int randomX = arc4random() % _grid.width;
        int randomY = arc4random() % _grid.height;
        
        ((NodeModel *)[_grid getAtX:randomX y:randomY]).type = gkTypeMine;
    }
}

-(void) placeSpecificMines:(NSDictionary *)properties {
    NSArray *mines = [properties objectForKey:@"mines"];
    NSEnumerator *enumerator = [mines objectEnumerator];
    NSDictionary *mine;
    while ((mine = [enumerator nextObject])) {
        ((NodeModel *)[_grid 
                       getAtX:[[mine objectForKey:@"x"] intValue]
                       y:[[mine objectForKey:@"y"] intValue]]).type = gkTypeMine;
    }
}

-(int) determineValue:(NodeModel *)model {
    int tally = 0;
    NSArray *nodes = [_grid getPerimeterValues:model.x y:model.y radius:1];
    NSEnumerator *enumerator = [nodes objectEnumerator];
    NodeModel *pmodel;
    while ((pmodel = [enumerator nextObject])) {
        if (gkTypeMine == pmodel.type) {
            tally++;
        }
    }
    
    return tally;
}

@end