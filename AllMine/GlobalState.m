//
//  GlobalState.m
//  AllMine
//
//  Created by Benjamin Jordan on 7/7/12.
//  Copyright (c) 2012 TheGoldenMule. All rights reserved.
//

#import "GlobalState.h"
#import "GameKitHelper.h"

@interface GlobalState (Private)
-(void)authenticationChanged:(NSNotification *)notification;
@end

@implementation GlobalState

static GlobalState *_instance;

+(GlobalState *) defaultInstance {
    if (nil == _instance) {
        _instance = [[GlobalState alloc] init];
    }
    
    return _instance;
}

@synthesize authenticated;
@synthesize difficulty;
@synthesize action;
@synthesize completionType;

-(id) init {
    if ((self = [super init])) {
        self.difficulty = 1.0f;
        
        _energy = _biodiesel = 0;
        
        // listen for game center auth changes
        [[NSNotificationCenter defaultCenter] addObserver:self selector:@selector(authenticationChanged:) name:GKPlayerAuthenticationDidChangeNotificationName object:nil];
        
        // load userstate
        {
            int (^loadInt)(NSString *key) = ^int(NSString *key) {
                NSString *value = [[NSUserDefaults standardUserDefaults] stringForKey:key];
                if (nil == value) {
                    return 0;
                }
                
                NSNumberFormatter *formatter = [[[NSNumberFormatter alloc] init] autorelease];
                [formatter setNumberStyle:NSNumberFormatterDecimalStyle];
                return [[formatter numberFromString:value] intValue];
            };
            
            _totalBiodiesel = loadInt(@"totalBiodiesel");
            _totalAreaCleared = loadInt(@"totalAreaCleared");
        }
    }
    
    return self;
}

-(int) totalBiodiesel {
    return _totalBiodiesel;
}

-(void) setTotalBiodiesel:(int)totalBiodiesel {
    CCLOG(@"Setting total biodiesel : %i", totalBiodiesel);
    [[NSUserDefaults standardUserDefaults] setValue:[NSString stringWithFormat:@"%i", totalBiodiesel] forKey:@"totalBiodiesel"];
}

-(int) totalAreaCleared {
    return _totalAreaCleared;
}

-(void) setTotalAreaCleared:(int)totalAreaCleared {
    [[NSUserDefaults standardUserDefaults] setValue:[NSString stringWithFormat:@"%i", totalAreaCleared] forKey:@"totalAreaCleared"];
}

-(int)biodiesel {
    return _biodiesel;
}

-(int)energy {
    return _energy;
}

-(void)setBiodiesel:(int)biodiesel {
    if (_biodiesel == biodiesel) return;
    
    // take difference
    int difference = biodiesel - _biodiesel;
    if (difference > 0) {
        [self setTotalBiodiesel:self.biodiesel + difference];
    }
    
    _biodiesel = biodiesel;
    [[NSNotificationCenter defaultCenter]
     postNotification:[NSNotification notificationWithName:EVENT_BIODIESEL_UPDATE object:self]];
}

-(void)setEnergy:(int)energy {
    // clamp
    energy = energy > 100 ? 100 : energy < 0 ? 0 : energy;
    if (_energy == energy) return;
    
    _energy = energy;
    [[NSNotificationCenter defaultCenter]
     postNotification:[NSNotification notificationWithName:EVENT_ENERGY_UPDATE object:self]];
}

#pragma mark Private

-(void)authenticationChanged:(NSNotification *)notification {
    dispatch_async(dispatch_get_main_queue(), ^(void) {
        BOOL isAuthenticated = [GKLocalPlayer localPlayer].isAuthenticated;
        if (isAuthenticated && !_authenticated) {
            NSLog(@"User is now authenticated.");
            _authenticated = YES;
        } else if (isAuthenticated && !_authenticated) {
            NSLog(@"User is no longer authenticated.");
            _authenticated = NO;
        }
    });
}

@end
