//
//  ActionsLayer.m
//  AllMine
//
//  Created by Benjamin Jordan on 7/7/12.
//  Copyright 2012 TheGoldenMule. All rights reserved.
//

#import "ActionsLayer.h"
#import "ActionButton.h"
#import "NodeAction.h"
#import "GlobalState.h"
#import "GameUtils.h"
#import "InfoIcon.h"

@interface ActionsLayer (Private)
-(void)actionSelected:(NSNotification *)notification;
-(void)energyUpdate:(NSNotification *)notification;
-(void)biodieselUpdate:(NSNotification *)notification;

-(void)easeOpenMenu;
-(void)easeCloseMenu;

-(void)displayMenuFor:(ActionButton *)button;
-(void)placeInfoIcons;
@end

@implementation ActionsLayer

-(id) init {
    if ((self = [super init])) {
        _isOpening = _isClosing = _isOpen = false;
        
        CGSize rect = [[CCDirector sharedDirector] winSizeInPixels];
        
        // create bg
        _bg = [[RectangleLayer alloc] initWithWidth:200 height:rect.height];
        
        rect = [[CCDirector sharedDirector] winSize];
        _bg.position = ccp(
                           rect.width - 100,
                           0);
        [self addChild:_bg];
        
        // create + position the labels
        _energy = [CCLabelTTF labelWithString:@"E : " fontName:@"Helvetica" fontSize:14];
        _biodiesel = [CCLabelTTF labelWithString:@"B : " fontName:@"Helvetica" fontSize:14];
        _levelName = [CCLabelTTF labelWithString:@"Level" fontName:@"Helvetica" fontSize:16];
        
        _energy.position = ccp(
                               rect.width - 75,
                               rect.height - 10);
        _levelName.position = ccp(
                               rect.width / 2,
                               rect.height - 10);
        _biodiesel.position = ccp(
                               rect.width - 25,
                               rect.height - 10);
        [self addChild:_energy];
        [self addChild:_biodiesel];
        //[self addChild:_levelName];
        
        // listen for updates
        [[NSNotificationCenter defaultCenter] addObserver:self selector:@selector(biodieselUpdate:) name:EVENT_BIODIESEL_UPDATE object:nil];
        [[NSNotificationCenter defaultCenter] addObserver:self selector:@selector(energyUpdate:) name:EVENT_ENERGY_UPDATE object:nil];
        
        // make sure this atlas is added to the cache
        [[CCSpriteFrameCache sharedSpriteFrameCache] addSpriteFramesWithFile:@"ActionsLayer.plist"];
        
        // create a batch node
        _batchNode = [[CCSpriteBatchNode batchNodeWithFile:@"ActionsLayer.png"] retain];
        
        [self addChild:_batchNode];
        
        // actions
        NodeAction *scout =
        [[[NodeAction alloc] initFromDictionary:
         [NSDictionary dictionaryWithObjectsAndKeys:
          @"Scout", @"labelName",
          ACTION_SCOUT, @"action",
          [NSNumber numberWithInt:10], @"cost",
          [NSNumber numberWithInt:1], @"searchDepth", nil]] autorelease];
        
        NodeAction *scoutingParty =
        [[[NodeAction alloc] initFromDictionary:
         [NSDictionary dictionaryWithObjectsAndKeys:
          @"Scouting Party", @"labelName",
          ACTION_SCOUTING_PARTY, @"action",
          [NSNumber numberWithInt:20], @"cost",
          [NSNumber numberWithInt:5], @"searchDepth", nil]] autorelease];
        
        NodeAction *miner =
        [[[NodeAction alloc] initFromDictionary:
         [NSDictionary dictionaryWithObjectsAndKeys:
          @"Miner", @"labelName",
          ACTION_MINER, @"action",
          [NSNumber numberWithInt:5], @"cost",
          [NSNumber numberWithInt:1], @"fuelMultiplier",
          [NSNumber numberWithInt:0], @"energyMultiplier",
          [NSNumber numberWithInt:5], @"deployCount", nil]] autorelease];
        
        NodeAction *miningCrew =
        [[[NodeAction alloc] initFromDictionary:
         [NSDictionary dictionaryWithObjectsAndKeys:
          @"Mining Crew", @"labelName",
          ACTION_MINING_CREW, @"action",
          [NSNumber numberWithInt:10], @"cost",
          [NSNumber numberWithInt:3], @"fuelMultiplier",
          [NSNumber numberWithInt:0], @"energyMultiplier",
          [NSNumber numberWithInt:7], @"deployCount", nil]] autorelease];
        
        NodeAction *powerman =
        [[[NodeAction alloc] initFromDictionary:
         [NSDictionary dictionaryWithObjectsAndKeys:
          @"Power Man", @"labelName",
          ACTION_POWERMAN, @"action",
          [NSNumber numberWithInt:1], @"cost",
          [NSNumber numberWithInt:0], @"fuelMultiplier",
          [NSNumber numberWithInt:1], @"energyMultiplier",
          [NSNumber numberWithInt:5], @"deployCount", nil]] autorelease];
        
        NodeAction *powerstation =
        [[[NodeAction alloc] initFromDictionary:
         [NSDictionary dictionaryWithObjectsAndKeys:
          @"Power Station", @"labelName",
          ACTION_POWERSTATION, @"action",
          [NSNumber numberWithInt:20], @"cost",
          [NSNumber numberWithInt:0], @"fuelMultiplier",
          [NSNumber numberWithInt:5], @"energyMultiplier",
          [NSNumber numberWithInt:7], @"deployCount", nil]] autorelease];
        
        NodeAction *demo =
        [[[NodeAction alloc] initFromDictionary:
         [NSDictionary dictionaryWithObjectsAndKeys:
          @"Demo Man", @"labelName",
          ACTION_DEMOLITIONS_EXPERT, @"action",
          [NSNumber numberWithInt:20], @"cost",
          [NSNumber numberWithInt:0], @"fuelMultiplier",
          [NSNumber numberWithInt:0], @"energyMultiplier",
          [NSNumber numberWithInt:0], @"deployCount", nil]] autorelease];
        
        // store all node actions
        _nodeActions = [[NSArray arrayWithObjects:
                         scout,
                         scoutingParty,
                         miner,
                         miningCrew,
                         powerman,
                         powerstation,
                         demo, nil] retain];
        
        // create info
        _actionInfoIcons = [[NSMutableArray arrayWithObjects:
                            [InfoIcon iconWithIconName:@"Search"],
                            [InfoIcon iconWithIconName:@"Cost"],
                            [InfoIcon iconWithIconName:@"Fuel"],
                            [InfoIcon iconWithIconName:@"Energy"],
                            [InfoIcon iconWithIconName:@"Lifespan"], nil] retain];
        
        // place info icons
        [self placeInfoIcons];
        
        // create buttons
        NSMutableArray *buttons = [NSMutableArray array];
        NSEnumerator *enumerator = [_nodeActions objectEnumerator];
        NodeAction *action;
        while ((action = [enumerator nextObject])) {
            [buttons addObject:
             [[[ActionButton alloc] initWithNodeAction:action] autorelease]];
        }
        
        // display buttons
        [self displayButtons:buttons];
        
        // listen for buttons
        [[NSNotificationCenter defaultCenter] addObserver:self selector:@selector(actionSelected:) name:EVENT_ACTION_SELECTED object:nil];
    }
    
    return self;
}

-(void)dealloc {
    CCLOG(@"Deallocating %@", self);
    
    [[NSNotificationCenter defaultCenter] removeObserver:self];
    
    [_batchNode release];
    [_nodeActions release];
    [_actionInfoIcons release];
    [_buttons release];
    [_bg release];
    
    [super dealloc];
}

-(void)displayButtons:(NSArray *)actionButtons {
    // display down the right hand side
    if (nil != _buttons) [_buttons release];
    _buttons = [actionButtons retain];
    
    // calculate offset to center about center
    CGSize size = [[CCDirector sharedDirector] winSize];
    const int x_padding = 50;
    const int y_padding = 40;
    const int y_offset = (size.height - y_padding * ([_buttons count] - 1)) / 2;
    
    for (int i = 0, len = [_buttons count]; i < len; i++) {
        ActionButton *button = [_buttons objectAtIndex:i];
        button.position = ccp(
                              size.width - x_padding,
                              size.height - (y_padding * i + y_offset));
        [self addChild:button];
    }
}

-(void)build:(NSDictionary *)properties {
    [_energy setString:
     [NSString stringWithFormat:@"E : %@", [properties objectForKey:@"starting_energy"]]];
    [_biodiesel setString:
     [NSString stringWithFormat:@"B : %@", [properties objectForKey:@"starting_biodiesel"]]];
    [_levelName setString:
     [properties objectForKey:@"name"]];
}

-(void)onEnter {
    [[CCTouchDispatcher sharedDispatcher] addTargetedDelegate:self priority:0 swallowsTouches:YES];
    [super onEnter];
}

-(void)onExit {
    [[CCTouchDispatcher sharedDispatcher] removeDelegate:self];
    [super onExit];
}

#pragma mark CCTargetedTouchDelegate

-(BOOL)ccTouchBegan:(UITouch *)touch withEvent:(UIEvent *)event {
    CGPoint location = [GameUtils locationInNodeSpace:touch target:_bg];
    CGRect rect = CGRectMake(0, 0, _bg.width, _bg.height);
    if (CGRectContainsPoint(rect, location)) {
        // check buttons!
        
        return YES;
    } else {
        [self easeCloseMenu];
    }
    
    return NO;
}

-(void)ccTouchMoved:(UITouch *)touch withEvent:(UIEvent *)event {
    
}

-(void)ccTouchEnded:(UITouch *)touch withEvent:(UIEvent *)event {
    
}

-(void)ccTouchCancelled:(UITouch *)touch withEvent:(UIEvent *)event {
    
}

#pragma mark Private

-(void)actionSelected:(NSNotification *)notification {
    // get the action button
    ActionButton *selectedButton = notification.object;
    
    // open menu
    [self displayMenuFor:selectedButton];
    [self easeOpenMenu];
    
    // select current, deselect others
    NSEnumerator *enumerator = [_buttons objectEnumerator];
    ActionButton *button;
    while ((button = [enumerator nextObject])) {
        button.selected = NO;
    }
    
    selectedButton.selected = YES;
}

-(void)energyUpdate:(NSNotification *)notification {
    [_energy setString:
     [NSString stringWithFormat:@"E : %i", [GlobalState defaultInstance].energy]];
}

-(void)biodieselUpdate:(NSNotification *)notification {
    [_biodiesel setString:
     [NSString stringWithFormat:@"B : %i", [GlobalState defaultInstance].biodiesel]];
}
static const float _time = 0.1;
-(void)easeOpenMenu {
    if (_isOpening) return;
    _isClosing = NO;
    _isOpening = YES;
    _isOpen = YES;
    
    NSEnumerator *enumerator = [_buttons objectEnumerator];
    ActionButton *button;
    CGSize size = [[CCDirector sharedDirector] winSize];
    while ((button = [enumerator nextObject])) {
        // kill previous actions
        [[CCActionManager sharedManager] removeAllActionsFromTarget:button];
        
        // run new action
        [button runAction:
            [CCActionTween actionWithDuration:_time key:@"x" from:button.x to:size.width - 90]];
    }
    
    enumerator = [_actionInfoIcons objectEnumerator];
    InfoIcon *icon;
    while ((icon = [enumerator nextObject])) {
        // kill previous actions
        [[CCActionManager sharedManager] removeAllActionsFromTarget:icon];
        
        // run new action
        [icon runAction:
         [CCActionTween actionWithDuration:_time key:@"x" from:icon.x to:size.width - 35]];
    }
    
    // tween the rectangle
    [_bg runAction:
     [CCSpawn actions:
      [CCActionTween actionWithDuration:_time key:@"width" from:_bg.width to:250],
      [CCActionTween actionWithDuration:_time key:@"x" from:_bg.x to:size.width - 125], nil]];
    
}

-(void)easeCloseMenu {
    if (_isClosing) return;
    _isOpening = _isOpen = NO;
    _isClosing = NO;
    
    NSEnumerator *enumerator = [_buttons objectEnumerator];
    ActionButton *button;
    CGSize size = [[CCDirector sharedDirector] winSize];
    while ((button = [enumerator nextObject])) {
        // kill previous actions
        [[CCActionManager sharedManager] removeAllActionsFromTarget:button];
        
        // run new action
        [button runAction:
         [CCActionTween actionWithDuration:_time key:@"x" from:button.x to:size.width - 50]];
    }
    
    enumerator = [_actionInfoIcons objectEnumerator];
    InfoIcon *icon;
    while ((icon = [enumerator nextObject])) {
        // kill previous actions
        [[CCActionManager sharedManager] removeAllActionsFromTarget:icon];
        
        // run new action
        [icon runAction:
         [CCActionTween actionWithDuration:_time key:@"x" from:icon.x to:size.width + 35]];
    }
    
    // tween the rectangle
    [_bg runAction:
     [CCSpawn actions:
      [CCActionTween actionWithDuration:_time key:@"width" from:_bg.width to:200],
      [CCActionTween actionWithDuration:_time key:@"x" from:_bg.x to:size.width - 100], nil]];
}

-(void)displayMenuFor:(ActionButton *)button {
    NSDictionary *props = button.action.properties;
    
    ((InfoIcon *)[_actionInfoIcons objectAtIndex:4]).value = [[props valueForKey:@"cost"] intValue];
    ((InfoIcon *)[_actionInfoIcons objectAtIndex:3]).value = [[props valueForKey:@"searchDepth"] intValue];
    ((InfoIcon *)[_actionInfoIcons objectAtIndex:1]).value = [[props valueForKey:@"fuelMultiplier"] intValue];
    ((InfoIcon *)[_actionInfoIcons objectAtIndex:2]).value = [[props valueForKey:@"energyMultiplier"] intValue];
    ((InfoIcon *)[_actionInfoIcons objectAtIndex:0]).value = [[props valueForKey:@"deployCount"] intValue];
}

-(void)placeInfoIcons {
    CGSize size = [[CCDirector sharedDirector] winSize];
    const int x_padding = 35;
    const int y_padding = 50;
    const int y_offset = (size.height - y_padding * ([_actionInfoIcons count] - 1)) / 2;
    
    // lay them out...
    InfoIcon *icon;
    for (int i = 0, len = [_actionInfoIcons count]; i < len; i++) {
        icon = [_actionInfoIcons objectAtIndex:i];
        icon.position = ccp(
                            size.width + x_padding,
                            y_offset + (i * y_padding));
        
        [self addChild:icon];
    }
}

@end