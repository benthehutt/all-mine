//
//  GameUtils.m
//  AllMine
//
//  Created by Benjamin Jordan on 7/9/12.
//  Copyright (c) 2012 TheGoldenMule. All rights reserved.
//

#import "GameUtils.h"
#import "Constants.h"

@implementation GameUtils

+(BOOL)nodeContainsTouchLocation:(UITouch *)touch target:(CCNode *)target {
    CGPoint locationInNodeSpace = [self locationInNodeSpace:touch target:target];
    CGRect bbox = CGRectMake(- CELL_DIM / 2, - CELL_DIM / 2, CELL_DIM, CELL_DIM);
    
    return CGRectContainsPoint(bbox, locationInNodeSpace);
}

+(BOOL)containsTouchLocation:(UITouch *)touch target:(CCNode *)target {
    CGPoint locationInNodeSpace = [self locationInNodeSpace:touch target:target];
    
    CGRect bbox = CGRectMake(0, 0, 
                             target.contentSize.width, 
                             target.contentSize.height);
    
    return CGRectContainsPoint(bbox, locationInNodeSpace);
}

+(CGPoint)locationInNodeSpace:(UITouch *)touch target:(CCNode *)target {
    CCDirector* director = [CCDirector sharedDirector];
    CGPoint touchLocation = [touch locationInView:director.openGLView];
    CGPoint locationGL = [director convertToGL:touchLocation];
    return [target convertToNodeSpace:locationGL];
}

@end
