//
//  Constants.h
//  AllMine
//
//  Created by Benjamin Jordan on 7/7/12.
//  Copyright (c) 2012 TheGoldenMule. All rights reserved.
//

#ifndef AllMine_Constants_h
#define AllMine_Constants_h

typedef enum {
    gkTypeNormal,
    gkTypeMine
} NodeType;

typedef enum {
    gkStateUnknown,
    gkStateNeutral,
    gkStateDeployed,
    gkStateDestroyed
} NodeState;

#define ACTION_SCOUT @"Scout"
#define ACTION_SCOUTING_PARTY @"ScoutingParty"

#define ACTION_MINER @"Miner"
#define ACTION_MINING_CREW @"MiningCrew"

#define ACTION_POWERMAN @"PowerMan"
#define ACTION_POWERSTATION @"PowerStation"

#define ACTION_DEMOLITIONS_EXPERT @"DemolitionExpert"

#define CELL_DIM 50

#define EVENT_BIODIESEL_UPDATE @"BiodieselUpdate"
#define EVENT_ENERGY_UPDATE @"EnergyUpdate"
#define EVENT_NODE_SELECTED @"NodeSelected"
#define EVENT_ACTION_SELECTED @"ActionSelected"

#define COMPLETIONTYPE_CLEAR @"cleararea"
#define COMPLETIONTYPE_BIODIESEL @"biodieselcollected"
#define COMPLETIONTYPE_MINES @"miniescleared"

#endif