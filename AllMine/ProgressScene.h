//
//  ProgressScene.h
//  AllMine
//
//  Created by Benjamin Jordan on 7/18/12.
//  Copyright 2012 TheGoldenMule. All rights reserved.
//

#import <Foundation/Foundation.h>
#import "cocos2d.h"
#import "GridLayer.h"

@interface ProgressScene : CCScene {
    GridLayer *_gridLayer;
}

+(ProgressScene *)scene;

@end