//
//  GameScene.h
//  AllMine
//
//  Created by Benjamin Jordan on 7/7/12.
//  Copyright (c) 2012 TheGoldenMule. All rights reserved.
//

#import "CCScene.h"
#import "GridLayer.h"
#import "NodeSprite.h"
#import "ActionsLayer.h"

@interface GameScene : CCScene {
    NSDictionary *_properties;
    
    GridLayer *_gridLayer;
    ActionsLayer *_actionsLayer;
    
    NodeSprite *_currentNode;
}

+(GameScene *)sceneWithRandomLevel;

-(void)build:(NSString *)levelName;

@end
