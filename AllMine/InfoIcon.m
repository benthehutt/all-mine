//
//  InfoIcon.m
//  AllMine
//
//  Created by Benjamin Jordan on 7/14/12.
//  Copyright 2012 TheGoldenMule. All rights reserved.
//

#import "InfoIcon.h"


@implementation InfoIcon

+(InfoIcon *)iconWithIconName:(NSString *)iconName {
    return [[[InfoIcon alloc] initWithIconName:iconName] autorelease];
}

-(id)initWithIconName:(NSString *)iconName {
    if ((self = [super init])) {
        // add bg
        [self addChild:[CCSprite spriteWithSpriteFrameName:@"Info.png"]];
        
        // add icon
        [self addChild:[CCSprite spriteWithSpriteFrameName:[NSString stringWithFormat:@"%@.png", iconName]]];
        
        // add label
        _valueLabel = [[CCLabelTTF labelWithString:@"0" fontName:@"Helvetica" fontSize:12] retain];
        _valueLabel.position = ccp(0, -20);
        [self addChild:_valueLabel];
        
        _value = 0;
    }
    
    return self;
}

-(void) dealloc {
    CCLOG(@"Deallocating %@", self);
    
    [_valueLabel release];
    
    [super dealloc];
}

-(int) value {
    return _value;
}

-(void) setValue:(int)value {
    if (_value == value) return;
    
    _value = value;
    
    [_valueLabel setString:[NSString stringWithFormat:@"%i", _value]];
}

-(int) x {
    return self.position.x;
}

-(void) setX:(int)x {
    self.position = ccp(x, self.position.y);
}

@end