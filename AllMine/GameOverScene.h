//
//  GameOverScene.h
//  AllMine
//
//  Created by Benjamin Jordan on 7/8/12.
//  Copyright 2012 TheGoldenMule. All rights reserved.
//

#import <Foundation/Foundation.h>
#import "cocos2d.h"

@interface GameOverScene : CCScene {
    CCMenu *_menu;
}

+(GameOverScene *)scene;

@end
