//
//  Array2.m
//  AllMine
//
//  Created by Benjamin Jordan on 7/7/12.
//  Copyright (c) 2012 TheGoldenMule. All rights reserved.
//

#import "cocos2d.h"
#import "Array2.h"

@interface Array2 (Private)
-(int)getIndex:(int)x y:(int)y;
@end

@implementation Array2

-(Array2 *)arrayWithWidth:(int) width height:(int)height {
    return [[[Array2 alloc] initWithWidth:width height:height] autorelease];
}

@synthesize width = _width;
@synthesize height = _height;

-(id)initWithWidth:(int)width height:(int)height {
    if ((self = [super init])) {
        _height = height;
        _width = width;
        
        // create array
        _array = [[NSMutableArray alloc] init];
        
        // prefil it
        for (int i = 0, len = _width * _height; i < len; i++) {
            [_array addObject:[NSValue valueWithPointer:nil]];
        }
    }
    
    return self;
}

-(void)dealloc {
    CCLOG(@"Deallocating %@", self);
    
    [_array release];
    
    [super dealloc];
}

-(id)setAtX:(int)x y:(int)y object:(id)object {
    // replace
    [_array replaceObjectAtIndex:[self getIndex:x y:y] withObject:object];
    
    return object;
}

-(id)getAtX:(int)x y:(int)y {
    if (x >= _width || x < 0 || y >= _height || y < 0) {
        return nil;
    }
    
    id value = [_array objectAtIndex:[self getIndex:x y:y]];
    
    if ([value isKindOfClass:[NSValue class]]) {
        return nil;
    }
    
    return value;
}

-(NSArray *)getPerimeterValues:(int)x y:(int)y radius:(int)radius {
    NSMutableArray *values = [NSMutableArray array];
    
    id value;
    for (int ix = -radius; ix < radius * 2; ix++) {
        for (int jy = -radius; jy < radius * 2; jy++) {
            value = [self getAtX:x + ix y:y + jy];
            if (nil != value) {
                [values addObject:value];
            }
        }     
    }
        
    return values;
}

#pragma mark Private

-(int)getIndex:(int)x y:(int)y {
    return x + _width * y;
}

@end
