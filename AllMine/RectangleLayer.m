//
//  RectangleLayer.m
//  AllMine
//
//  Created by Benjamin Jordan on 7/9/12.
//  Copyright (c) 2012 TheGoldenMule. All rights reserved.
//

#import "RectangleLayer.h"

@implementation RectangleLayer

@synthesize width = _width;
@synthesize height = _height;

-(id)initWithWidth:(int)width height:(int)height {
    if ((self = [super init])) {
        _width = width;
        _height = height;
    }
    
    return self;
}

-(void)dealloc {
    CCLOG(@"Deallocating %@", self);
    
    [super dealloc];
}

-(float)x {
    return self.position.x;
}

-(void)setX:(float)x {
    self.position = ccp(x, self.position.y);
}

-(float)y {
    return self.position.x;
}

-(void)setY:(float)y {
    self.position = ccp(self.position.x, y);
}

-(void)draw {
    glEnable(GL_LINE_SMOOTH);
    glColor4ub(102, 102, 102, 255);
    glLineWidth(2);
    CGPoint vertices[] = {
        ccp(0, _height),
        ccp(_width, _height),
        ccp(_width, 0),
        ccp(0, 0)
    };
    ccFillPoly(vertices, 4, YES);
}

@end
