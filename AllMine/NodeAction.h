//
//  NodeAction.h
//  AllMine
//
//  Created by Benjamin Jordan on 7/7/12.
//  Copyright (c) 2012 TheGoldenMule. All rights reserved.
//

#import <Foundation/Foundation.h>
#import "Constants.h"

@interface NodeAction : NSObject {
    NSString *_action;
    NSString *_labelName;
    NSMutableDictionary *_properties;
    int cost;
}

@property (readonly, nonatomic) NSString *action;
@property (readonly, nonatomic) NSString *labelName;
@property (readonly, nonatomic) NSDictionary *properties;
@property (readonly, nonatomic) int cost;

-(id) initFromDictionary:(NSDictionary *)properties;

@end
