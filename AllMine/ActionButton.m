//
//  ActionButton.m
//  AllMine
//
//  Created by Benjamin Jordan on 7/7/12.
//  Copyright 2012 TheGoldenMule. All rights reserved.
//

#import "ActionButton.h"

@interface ActionButton (Private)
-(BOOL)containsTouchLocation:(UITouch *)touch;
@end

@implementation ActionButton

@synthesize action = _action;

-(id)initWithNodeAction:(NodeAction *)action {
    if ((self = [super initWithSpriteFrameName:
                 [NSString stringWithFormat:@"%@.png", action.action]])) {
        _action = [action retain];
        _selected = NO;
        _originalColor = color_;
    }
    
    return self;
}

-(void) dealloc {
    CCLOG(@"Deallocating %@", self);
    
    [_action release];
    
    [super dealloc];
}

-(BOOL)selected {
    return _selected;
}

-(void)setSelected:(BOOL)selected {
    if (_selected == selected) return;
    
    _selected = selected;
    
    if (_selected) {
        self.color = ccc3(50, 50, 50);
    } else {
        self.color = _originalColor;
    }
}

-(float)x {
    return self.position.x;
}

-(void)setX:(float)x {
    self.position = ccp(x, self.position.y);
}

-(float)y {
    return self.position.x;
}

-(void)setY:(float)y {
    self.position = ccp(self.position.x, y);
}

-(void)onEnter {
    [[CCTouchDispatcher sharedDispatcher] addTargetedDelegate:self priority:0 swallowsTouches:YES];
    [super onEnter];
}

-(void)onExit {
    [[CCTouchDispatcher sharedDispatcher] removeDelegate:self];
    [super onExit];
}

#pragma mark CCTargetedTouchDelegate

- (BOOL)ccTouchBegan:(UITouch *)touch withEvent:(UIEvent *)event {
    if ([self containsTouchLocation:touch]) {
        // notify!
        [[NSNotificationCenter defaultCenter] 
         postNotification:[NSNotification
                           notificationWithName:EVENT_ACTION_SELECTED
                           object:self]];
        
        return YES;
    }
    
    return NO;
}

- (void)ccTouchMoved:(UITouch *)touch withEvent:(UIEvent *)event {
    
}

- (void)ccTouchEnded:(UITouch *)touch withEvent:(UIEvent *)event {
    
}

- (void)ccTouchCancelled:(UITouch *)touch withEvent:(UIEvent *)event {
    
}

#pragma mark Private

-(BOOL)containsTouchLocation:(UITouch *)touch {
    CCDirector* director = [CCDirector sharedDirector];
    CGPoint touchLocation = [touch locationInView:director.openGLView];
    CGPoint locationGL = [director convertToGL:touchLocation];
    CGPoint locationInNodeSpace = [self convertToNodeSpace:locationGL];
    
    CGRect bbox = CGRectMake(0, 0, 
                             self.contentSize.width, 
                             self.contentSize.height);
    
    return CGRectContainsPoint(bbox, locationInNodeSpace);
}

@end