//
//  GlobalState.h
//  AllMine
//
//  Created by Benjamin Jordan on 7/7/12.
//  Copyright (c) 2012 TheGoldenMule. All rights reserved.
//

#import <Foundation/Foundation.h>
#import "Constants.h"
#import "NodeAction.h"

@interface GlobalState : NSObject {
    int _biodiesel;
    int _energy;
    BOOL _authenticated;
    
    int _totalBiodiesel;
    int _totalAreaCleared;
}

+(GlobalState *) defaultInstance;

@property (nonatomic, readonly) BOOL authenticated;
@property (nonatomic) float difficulty;
@property (nonatomic, assign) NodeAction *action;
@property (nonatomic) int biodiesel;
@property (nonatomic) int energy;
@property (nonatomic, retain) NSString *completionType;

@property (nonatomic) int totalBiodiesel;
@property (nonatomic) int totalAreaCleared;

@end