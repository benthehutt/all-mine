//
//  ActionButton.h
//  AllMine
//
//  Created by Benjamin Jordan on 7/7/12.
//  Copyright 2012 TheGoldenMule. All rights reserved.
//

#import <Foundation/Foundation.h>
#import "cocos2d.h"
#import "NodeAction.h"

@interface ActionButton : CCSprite <CCTargetedTouchDelegate> {
    NodeAction *_action;
    BOOL _selected;
    ccColor3B _originalColor;
}

@property (readonly, nonatomic, retain) NodeAction *action;
@property (nonatomic) BOOL selected;
@property (nonatomic) float x;
@property (nonatomic) float y;

-(id)initWithNodeAction:(NodeAction *)action;

- (BOOL)ccTouchBegan:(UITouch *)touch withEvent:(UIEvent *)event;
- (void)ccTouchMoved:(UITouch *)touch withEvent:(UIEvent *)event;
- (void)ccTouchEnded:(UITouch *)touch withEvent:(UIEvent *)event;
- (void)ccTouchCancelled:(UITouch *)touch withEvent:(UIEvent *)event;

@end
