//
//  GridModel.h
//  AllMine
//
//  Created by Benjamin Jordan on 7/7/12.
//  Copyright (c) 2012 TheGoldenMule. All rights reserved.
//

#import <Foundation/Foundation.h>
#import "Array2.h"
#import "NodeModel.h"

@interface GridModel : NSObject {
    Array2 *_grid;
}

@property (nonatomic, readonly) int width;
@property (nonatomic, readonly) int height;

-(id) initWithProperties:(NSDictionary *)properties;
-(NodeModel *) getModelAtX:(int)x y:(int)y;
-(NSArray *) getPerimeterNodes:(NodeModel *)model radius:(int)radius;
-(NSDictionary *)getDeployTotals;
-(NodeModel *)findSafePatch;

@end