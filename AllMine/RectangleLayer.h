//
//  RectangleLayer.h
//  AllMine
//
//  Created by Benjamin Jordan on 7/9/12.
//  Copyright (c) 2012 TheGoldenMule. All rights reserved.
//

#import "CCSprite.h"
#import "cocos2d.h"

@interface RectangleLayer : CCSprite {
    
}

@property (nonatomic) int width;
@property (nonatomic) int height;
@property (nonatomic) float x;
@property (nonatomic) float y;

-(id)initWithWidth:(int)width height:(int)height;

@end
