//
//  GameScene.m
//  AllMine
//
//  Created by Benjamin Jordan on 7/7/12.
//  Copyright (c) 2012 TheGoldenMule. All rights reserved.
//

#import "GameScene.h"
#import "cocos2d.h"
#import "Constants.h"
#import "GridLayer.h"
#import "NodeSprite.h"
#import "NodeModel.h"
#import "ActionButton.h"
#import "GlobalState.h"
#import "GameOverScene.h"

@interface GameScene (Private)

-(NSDictionary *) getPropertiesForLevel:(NSString *)levelName;
-(void)scheduledUpdate;
-(BOOL)isLevelFailed;
-(BOOL)isLevelCompleted;
-(BOOL)isLevelOver;
-(void)nodeSelected:(NSNotification *)notification;
-(void)actionButtonSelected:(NSNotification *)notification;
-(void)energyChanged:(NSNotification *)notification;
-(void)biodieselChanged:(NSNotification *)notification;

@end

@implementation GameScene

+(GameScene *)sceneWithRandomLevel {
    GameScene *scene = [[[GameScene alloc] init] autorelease];
    
    // build random level
    [scene build:@"1-TutorialSweep"];
    
    return scene;
}

-(id) init {
    if ((self = [super init])) {
        CCLOG(@"New %@", self);
        
        _gridLayer = [[GridLayer alloc] init];
        [self addChild:_gridLayer];
        
        _actionsLayer = [[ActionsLayer alloc] init];
        [self addChild:_actionsLayer];
        
        // listen
        [[NSNotificationCenter defaultCenter] addObserver:self selector:@selector(energyChanged:) name:EVENT_ENERGY_UPDATE object:nil];
        [[NSNotificationCenter defaultCenter] addObserver:self selector:@selector(energyChanged:) name:EVENT_BIODIESEL_UPDATE object:nil];
    }
    
    return self;
}

-(void) dealloc {
    CCLOG(@"Deallocating %@", self);
    
    [_gridLayer release];
    [_actionsLayer release];
    
    [[NSNotificationCenter defaultCenter] removeObserver:self];
    
    [super dealloc];
}

-(void)build:(NSString *)levelName {
    // read in properties
    _properties = [self getPropertiesForLevel:levelName];
    
    if (nil == _properties) {
        CCLOG(@"Could not read level file: %@.plist", levelName);
        return;
    } else {
        CCLOG(@"Building %@.plist", levelName);
    }
    
    // now build the layers
    [_gridLayer build:_properties];
    [_actionsLayer build:_properties];
    
    // listen for notifications
    [[NSNotificationCenter defaultCenter] addObserver:self selector:@selector(nodeSelected:) name:EVENT_NODE_SELECTED object:nil];
    [[NSNotificationCenter defaultCenter] addObserver:self selector:@selector(actionButtonSelected:) name:EVENT_ACTION_SELECTED object:nil];
    
    // set initial values
    [GlobalState defaultInstance].completionType = [_properties objectForKey:@"completion_type"];
    [GlobalState defaultInstance].energy = [[_properties objectForKey:@"starting_energy"] intValue];
    [GlobalState defaultInstance].biodiesel = [[_properties objectForKey:@"starting_biodiesel"] intValue];
    
    // find a safe patch
    NodeModel *model = [_gridLayer.grid findSafePatch];
    [_gridLayer activateNodesAbout:model searchDepth:2];
    
    // now schedule updates
    [self schedule:@selector(scheduledUpdate) interval:1];
}

#pragma mark Private

 -(void)scheduledUpdate {
     // get totals
     NSDictionary *totals = [_gridLayer.grid getDeployTotals];
     [GlobalState defaultInstance].biodiesel += [[totals objectForKey:@"biodiesel"] intValue];
     [GlobalState defaultInstance].energy += [[totals objectForKey:@"energy"] intValue];
 }

-(BOOL)isLevelFailed {
    // check for failure
    return [GlobalState defaultInstance].energy <= 0;
}

-(BOOL)isLevelCompleted {
    NSString *completionType = [GlobalState defaultInstance].completionType;
    if ([completionType isEqualToString:COMPLETIONTYPE_CLEAR]) {
        if ([_gridLayer isClearOfMines]) {
            // level complete
            return YES;
        }
    }
    
    return NO;
}

-(BOOL)isLevelOver {
    if ([self isLevelCompleted]) {
        CCLOG(@"Level complete!");
        // for now, just use the same scene
        [[CCDirector sharedDirector] replaceScene:[GameOverScene scene]];
        return YES;
    }
    
    if ([self isLevelFailed]) {
        CCLOG(@"Level failed!");
        [[CCDirector sharedDirector] replaceScene:[GameOverScene scene]];
        return YES;
    }
    
    return NO;
}

-(NSDictionary *) getPropertiesForLevel:(NSString *)levelName {
    // get plist file
    NSString *fileName = [NSString stringWithFormat:@"%@.plist", levelName];
    NSString *plistPath;
    NSString *rootPath = [NSSearchPathForDirectoriesInDomains(NSDocumentDirectory, NSUserDomainMask, YES) objectAtIndex:0];
    plistPath = [rootPath stringByAppendingPathComponent:fileName];
    if (![[NSFileManager defaultManager] fileExistsAtPath:plistPath]) {
        plistPath = [[NSBundle mainBundle] pathForResource:levelName ofType:@"plist"];
    }
    
    return [NSDictionary dictionaryWithContentsOfFile:plistPath];
}

// this is called when a node has been selected
-(void)nodeSelected:(NSNotification *)notification {
    NodeAction *action = [GlobalState defaultInstance].action;
    if (nil == action) return;
    
    // set the node
    _currentNode = (NodeSprite *) notification.object;
    
    // only deal with unknown ones
    if (gkStateUnknown != _currentNode.model.state) return;
    
    // only allow clicking adjacent to already known spaces
    NSArray *nodes = [_gridLayer.grid getPerimeterNodes:_currentNode.model radius:1];
    NSEnumerator *enumerator = [nodes objectEnumerator];
    NodeModel *adjacent;
    BOOL valid = NO;
    while ((adjacent = [enumerator nextObject])) {
        if (gkStateUnknown != adjacent.state) {
            valid = YES;
            break;
        }
    }
    if (!valid) return;
    
    // can we afford this?
    if ([GlobalState defaultInstance].energy < action.cost) {
        CCLOG(@"Cannot afford action.");
        return;
    }
    
    // subtract cost
    [GlobalState defaultInstance].energy -= action.cost;
    
    // look at destroy first
    if (ACTION_DEMOLITIONS_EXPERT == action.action) {
        CCLOG(@"DESTROY!");
        // don't care what this space is, DESTROY IT!
        _currentNode.model.state = gkStateDestroyed;
        _currentNode.model.active = YES;
        return;
    }
    
    // mine?
    if (gkTypeMine == _currentNode.model.type) {
        // explode on yer!
        CCLOG(@"Hit a mine!");
        [GlobalState defaultInstance].energy -= 50;
        
        return;
    }
    
    // look at the action
    CCLOG(@"Action %@", action.action);
    if (ACTION_SCOUT == action.action
        || ACTION_SCOUTING_PARTY == action.action) {
        [_gridLayer activateNodesAbout:_currentNode.model searchDepth:[[action.properties valueForKey:@"searchDepth"] intValue]];
    } else if (ACTION_MINER == action.action
               || ACTION_MINING_CREW == action.action) {
        [_gridLayer activateNodesAbout:_currentNode.model searchDepth:1];
        _currentNode.model.deployedAction = action;
    } else if (ACTION_POWERSTATION == action.action
            || ACTION_POWERMAN == action.action) {
        [_gridLayer activateNodesAbout:_currentNode.model searchDepth:1];
        _currentNode.model.deployedAction = action;
    }
}

// this is called when an action button is pressed
-(void)actionButtonSelected:(NSNotification *)notification {
    ActionButton *button = notification.object;
    [GlobalState defaultInstance].action = button.action;
    
    CCLOG(@"Action button selected : %@", button.action.labelName);
}

-(void)energyChanged:(NSNotification *)notification {
    [self isLevelOver];
}

-(void)biodieselChanged:(NSNotification *)notification {
    [self isLevelOver];
}

@end