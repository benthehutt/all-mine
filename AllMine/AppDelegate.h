//
//  AppDelegate.h
//  AllMine
//
//  Created by Benjamin Jordan on 7/7/12.
//  Copyright TheGoldenMule 2012. All rights reserved.
//

#import <UIKit/UIKit.h>

@class RootViewController;

@interface AppDelegate : NSObject <UIApplicationDelegate> {
	UIWindow			*window;
	RootViewController	*viewController;
}

@property (nonatomic, retain) UIWindow *window;

@end
