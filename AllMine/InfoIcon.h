//
//  InfoIcon.h
//  AllMine
//
//  Created by Benjamin Jordan on 7/14/12.
//  Copyright 2012 TheGoldenMule. All rights reserved.
//

#import <Foundation/Foundation.h>
#import "cocos2d.h"

@interface InfoIcon : CCSprite {
    int _value;
    CCLabelTTF *_valueLabel;
}

@property (nonatomic) int value;
@property (nonatomic) int x;

+(InfoIcon *)iconWithIconName:(NSString *)iconName;
-(id)initWithIconName:(NSString *)iconName;

@end