//
//  ProgressScene.m
//  AllMine
//
//  Created by Benjamin Jordan on 7/18/12.
//  Copyright 2012 TheGoldenMule. All rights reserved.
//

#import "ProgressScene.h"
#import "GridLayer.h"

@interface ProgressScene (Private)

-(void)buildFromLevels:(NSArray *)levelNames;
-(void)nodeSelected:(NSNotification *)notification;

@end

@implementation ProgressScene

+(ProgressScene *)scene {
    return [[[ProgressScene alloc] init] autorelease];
}

-(id)init {
    if ((self = [super init])) {
        
        // create layers + listen
        {
            _gridLayer = [[GridLayer alloc] init];
            [self addChild:_gridLayer];
            
            [[NSNotificationCenter defaultCenter] addObserver:self selector:@selector(nodeSelected:) name:EVENT_NODE_SELECTED object:nil];
        }
        
        // get all level filenames
        NSMutableArray *levelNames = [NSMutableArray array];
        
        // get all filenames
        NSString *resourcePath = [[NSBundle mainBundle] resourcePath];
        NSError *error;
        NSArray *contents = [[NSFileManager defaultManager] contentsOfDirectoryAtPath:resourcePath error:&error];
        
        // pull out only levels
        NSEnumerator *enumerator = [contents objectEnumerator];
        NSString *levelName;
        NSRegularExpression *expression = [NSRegularExpression regularExpressionWithPattern:@"^\\d{1,2}-\\w+\\.plist$" options:NSRegularExpressionSearch error:&error];
        while ((levelName = [enumerator nextObject])) {
            if ([expression numberOfMatchesInString:levelName options:NSMatchingAnchored range:NSMakeRange(0, [levelName length])] > 0) {
                [levelNames addObject:levelName];
            }
        }
        
        // build the scene, one button for each level
        [self buildFromLevels:levelNames];
    }
    
    return self;
}

-(void)dealloc {
    CCLOG(@"Deallocating %@", self);
    
    [_gridLayer release];
    
    [super dealloc];
}

#pragma mark Private

-(void)buildFromLevels:(NSArray *)levelNames {
    NSMutableDictionary *properties = [NSMutableDictionary dictionary];
    [properties setObject:[NSNumber numberWithInt:10] forKey:@"grid_width"];
    [properties setObject:[NSNumber numberWithInt:10] forKey:@"grid_height"];
    
    NSMutableArray *mines = [NSMutableArray array];
    for (int i = 0, len = [levelNames count]; i < len; i++) {
        [mines addObject:[NSDictionary dictionaryWithObjectsAndKeys:
                          [NSNumber numberWithInt:(i * 2) % 5], @"x",
                          [NSNumber numberWithInt:floorf(i / 5)], @"y",
                          @"key", [levelNames objectAtIndex:i],
                          nil]];
    }
    [properties setObject:mines forKey:@"mines"];
    
    [_gridLayer build:properties];
}

-(void)nodeSelected:(NSNotification *)notification {
    
}

@end