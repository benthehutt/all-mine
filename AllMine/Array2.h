//
//  Array2.h
//  AllMine
//
//  Created by Benjamin Jordan on 7/7/12.
//  Copyright (c) 2012 TheGoldenMule. All rights reserved.
//

#import <Foundation/Foundation.h>

@interface Array2 : NSObject {
    int _width;
    int _height;
    NSMutableArray *_array;
}

-(Array2 *)arrayWithWidth:(int) width height:(int)height;

@property (readonly) int width;
@property (readonly) int height;

-(id)initWithWidth:(int)width height:(int)height;
-(id)setAtX:(int)x y:(int)y object:(id)object;
-(id)getAtX:(int)x y:(int)y;
-(NSArray *)getPerimeterValues:(int)x y:(int)y radius:(int)radius;

@end
