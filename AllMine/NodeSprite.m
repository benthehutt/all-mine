//
//  NodeSprite.m
//  AllMine
//
//  Created by Benjamin Jordan on 7/7/12.
//  Copyright (c) 2012 TheGoldenMule. All rights reserved.
//

#import "cocos2d.h"
#import "NodeSprite.h"

@interface NodeSprite (Private)
-(void)swapFrames;
@end

@implementation NodeSprite

@synthesize model = _model;

-(id) initWithModel:(NodeModel *)model {
    if ((self = [super init])) {
        _model = [model retain];
        
        // set the position
        self.position = ccp(
                            model.x * CELL_DIM + CELL_DIM / 2,
                            model.y * CELL_DIM + CELL_DIM / 2);
        
        _current = [[CCSprite spriteWithSpriteFrameName:SPRITE_UNEXPLORED] retain];
        _next = [[CCSprite alloc] init];
        
        // add current
        [self addChild:_current];
    }
                 
    return self;
}

-(void) dealloc {
    CCLOG(@"Deallocating %@", self);
    [_model release];
    [_current release];
    [_next release];
    
    [super dealloc];
}

-(void) invalidate {
    if (_model.active) {
        // change to explored
        [_next setDisplayFrame:
         [[CCSpriteFrameCache sharedSpriteFrameCache] spriteFrameByName:SPRITE_EXPLORED]];
        
        // create and add the label
        if (_model.value > 0) {
            CCLabelTTF *label = [CCLabelTTF
             labelWithString:[NSString stringWithFormat:@"%i", _model.value]
             fontName:@"Helvetica"
             fontSize:20];
            label.position = ccp(CELL_DIM / 2, CELL_DIM / 2);
            [_next addChild:label];
        }
    } else {
        [_next setDisplayFrame:
         [[CCSpriteFrameCache sharedSpriteFrameCache] spriteFrameByName:SPRITE_UNEXPLORED]];
    }
    
    [self swapFrames];
}

-(void) onDown {
    
}

#pragma mark Private

-(void)swapFrames {
    const float swapSpeed = 0.1f;
    
    void (^outFinished)(void) = ^ {
        [self removeChild:_current cleanup:NO];
        
        _next.scale = 0;
        [self addChild:_next];
        
        // scale back out!
        [_next runAction:[CCSequence actions:
                          [CCScaleTo actionWithDuration:swapSpeed scale:1],
                          [CCCallBlock actionWithBlock:
                           ^ {
                               // swap 'em back
                               CCSprite *temp = _current;
                               _current = _next;
                               _next = temp;
                           }], nil]];
    };
    
    // tween out
    [_current runAction:[CCSequence actions:
     [CCScaleTo actionWithDuration:swapSpeed scale:0],
     [CCCallBlock actionWithBlock:outFinished],
     nil]];
}

@end