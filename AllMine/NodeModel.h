//
//  NodeModel.h
//  AllMine
//
//  Created by Benjamin Jordan on 7/7/12.
//  Copyright (c) 2012 TheGoldenMule. All rights reserved.
//

#import <Foundation/Foundation.h>
#import "Constants.h"
#import "NodeAction.h"

@interface NodeModel : NSObject {
    int _x;
    int _y;
    NodeAction *_deployedAction;
}

@property (readonly, nonatomic) int x;
@property (readonly, nonatomic) int y;
@property (readwrite, nonatomic) int value;
@property (readwrite, nonatomic) NodeType type;
@property (readwrite, nonatomic) NodeState state;
@property (readwrite, nonatomic) BOOL active;
@property (nonatomic, assign) NodeAction *deployedAction;
@property (nonatomic) int deployCount;

+(NodeModel *)model;
-(id) initAtX:(int)x y:(int)y;

@end
