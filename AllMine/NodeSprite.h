//
//  NodeSprite.h
//  AllMine
//
//  Created by Benjamin Jordan on 7/7/12.
//  Copyright (c) 2012 TheGoldenMule. All rights reserved.
//

#import "CCSprite.h"
#import "NodeModel.h"
#import "cocos2d.h"

#define SPRITE_MINE @"Mine.png"
#define SPRITE_SELECTED @"Selected.png"
#define SPRITE_EXPLORED @"Explored.png"
#define SPRITE_UNEXPLORED @"Unexplored.png"

@interface NodeSprite : CCSprite {
    NodeModel *_model;
    
    CCSprite *_current;
    CCSprite *_next;
}

@property (readonly, nonatomic, retain) NodeModel *model;

-(id) initWithModel:(NodeModel *)model;
-(void) invalidate;
-(void) onDown;

@end
