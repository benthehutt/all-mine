//
//  NodeModel.m
//  AllMine
//
//  Created by Benjamin Jordan on 7/7/12.
//  Copyright (c) 2012 TheGoldenMule. All rights reserved.
//

#import "NodeModel.h"
#import "cocos2d.h"

@implementation NodeModel {
    
}

@synthesize x = _x;
@synthesize y = _y;
@synthesize value;
@synthesize type;
@synthesize state;
@synthesize active;
@synthesize deployCount;

+(NodeModel *) model {
    return [[[NodeModel alloc] init] autorelease];
}

-(id) init {
    if ((self = [super init])) {
        _x = 0;
        _y = 0;
        
        value = 0;
        type = gkTypeNormal;
        state = gkStateUnknown;
        active = NO;
    }
    
    return self;
}

-(id) initAtX:(int)x y:(int)y {
    if ((self = [self init])) {
        _x = x;
        _y = y;
    }
    return self;
}

-(void)dealloc {
    CCLOG(@"Deallocating %@", self);
    
    [super dealloc];
}

-(void)setDeployedAction:(NodeAction *)deployedAction {
    _deployedAction = deployedAction;
    
    // reset this...
    deployCount = [[_deployedAction.properties objectForKey:@"deployCount"] intValue];
}

-(NodeAction *)deployedAction {
    return _deployedAction;
}

@end
