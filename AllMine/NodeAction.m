//
//  NodeAction.m
//  AllMine
//
//  Created by Benjamin Jordan on 7/7/12.
//  Copyright (c) 2012 TheGoldenMule. All rights reserved.
//

#import "cocos2d.h"
#import "NodeAction.h"

@implementation NodeAction

@synthesize action = _action;
@synthesize labelName = _labelName;
@synthesize properties = _properties;
@synthesize cost = _cost;

-(id) initFromDictionary:(NSDictionary *)properties {
    if ((self = [super init])) {
        _action = [properties valueForKey:@"action"];
        _labelName = [properties valueForKey:@"labelName"];
        _properties = [[NSMutableDictionary dictionaryWithDictionary:properties] retain];
        _cost = [[properties valueForKey:@"cost"] intValue];
    }
    
    return self;
}

-(void) dealloc {
    CCLOG(@"Deallocating %@", self);
    
    [_properties release];
    
    [super dealloc];
}

@end
