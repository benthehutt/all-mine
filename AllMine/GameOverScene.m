//
//  GameOverScene.m
//  AllMine
//
//  Created by Benjamin Jordan on 7/8/12.
//  Copyright 2012 TheGoldenMule. All rights reserved.
//

#import "GameOverScene.h"
#import "GameScene.h"
#import "MainMenuScene.h"

@interface GameOverScene (Private)

-(void)retry;
-(void)mainMenu;

@end

@implementation GameOverScene

+(GameOverScene *)scene {
    return [[[GameOverScene alloc] init] autorelease];
}

-(id) init {
    if ((self = [super init])) {
        CCMenuItemLabel *retry = [CCMenuItemLabel itemWithLabel:[CCLabelTTF labelWithString:@"Retry" fontName:@"Helvetica" fontSize:20]  target:self selector:@selector(retry)];
        retry.position = ccp(retry.position.x, retry.position.y - 20);
        CCMenuItemLabel *mainMenu = [CCMenuItemLabel itemWithLabel:[CCLabelTTF labelWithString:@"Main Menu" fontName:@"Helvetica" fontSize:20]  target:self selector:@selector(mainMenu)];
        mainMenu.position = ccp(mainMenu.position.x, mainMenu.position.y + 20);
        _menu = [[CCMenu menuWithItems:retry, mainMenu, nil] retain];
        
        [self addChild:_menu];
    }
    
    return self;
}

-(void)dealloc {
    CCLOG(@"Dealloc %@", self);
       
    [_menu release];
    
    [super dealloc];
}

-(void)retry {
    [[CCDirector sharedDirector] replaceScene:[GameScene sceneWithRandomLevel]];
}

 -(void)mainMenu {
     [[CCDirector sharedDirector] replaceScene:[MainMenuScene scene]];
 }
@end