//
//  Node.h
//  AllMine
//
//  Created by Benjamin Jordan on 7/8/12.
//  Copyright (c) 2012 __MyCompanyName__. All rights reserved.
//

#import <Foundation/Foundation.h>
#import "NodeSprite.h"

@interface Node : NSObject

@property (nonatomic, retain) NodeSprite *sprite;
@property (nonatomic, retain) NodeModel *model;

@end
